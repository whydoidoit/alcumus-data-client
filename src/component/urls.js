export let url = (window ? window.url : global ? global.url : "http://localhost:3000")

export function setRemoteDataUrl(newUrl) {
    url = newUrl
}
