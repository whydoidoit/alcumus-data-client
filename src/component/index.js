import "regenerator-runtime/runtime"
import DataSource from "./datasource"
import RemoteDataSource from "./remote-datasource"
import {enable as enableRemoteMutations, disable as disableRemoteMutations, flush as flushRemoteMutations} from "./remote-mutations"
import {track} from "./datasource-mutations"
import {url as remoteDataUrl, setRemoteDataUrl} from "./urls"

export {DataSource, RemoteDataSource, enableRemoteMutations, disableRemoteMutations, flushRemoteMutations, track, remoteDataUrl, setRemoteDataUrl}
export default RemoteDataSource

