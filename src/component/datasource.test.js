import DataSource, {dataSources} from "./datasource"
import storage from "localforage"

global.fetch = require("jest-fetch-mock")

const DUMMY_ROWS = []
for (let i = 0; i < 1000; i++) {
    DUMMY_ROWS.push({_id: `${i}:test`, a: Math.floor(Math.random() * 200)})
}

const SAMPLE_ROWS = () => [
    {_id: 1, a: 1},
    {_id: 2, a: 10},
    {_id: 3, a: 6},
    {_id: 4, a: 2}
]

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}

describe("Data Store", function () {
    beforeAll(async function () {
        await storage.removeItem(`settings-test`)
    })
    beforeEach(()=>{
        fetch.resetMocks()
    })
    afterEach(() => {
        Object.entries(DataSource.byName).forEach(([, dataSource]) => {
            dataSource.destroy()
        })
    })
    it("should be able to set a number of rows", async function () {
        let source = new DataSource("test")
        await source.set(DUMMY_ROWS)
    })
    it("should be able to get a named data source", async function () {
        let source = new DataSource("test")
        await source.set(DUMMY_ROWS)
        expect(DataSource.get("test")).toEqual(source)
    })
    it("should fire a refreshed event", async function () {
        const cb = jest.fn()
        let source = new DataSource("test")
        source.on("refreshed", cb)
        await source.set(DUMMY_ROWS)
        await delay(40)
        expect(cb).toHaveBeenCalled()
    })
    it("should fire a refreshed event on a modification", async function () {
        const cb = jest.fn()
        let source = new DataSource("test")
        await source.set(DUMMY_ROWS)
        await delay()
        source.on("refreshed", cb)
        await source.append({a: 1, _id: "1001:test"})
        await delay(40)
        expect(cb).toHaveBeenCalled()
    })
    it("should be able to get a named data source, creating it", async function () {
        let source = DataSource.get("test")
        await source.set(DUMMY_ROWS)
        expect(DataSource.get("test")).toEqual(source)
    })
    it("should be able to get a row by number", async function () {
        let source = new DataSource("test")
        await source.set(DUMMY_ROWS)
        let row = await source.get(1)
        expect(row._id).toEqual("1:test")
        expect((await source.get("10:test"))._id).toEqual("10:test")
    })
    it("should fail to get an illegal row", async function () {
        let source = new DataSource("test")
        await source.set(DUMMY_ROWS)
        await expect(source.get("banana")).rejects.toThrow("Invalid row")
    })
    it("should uncache blocks after a timeout", async function () {
        let source = new DataSource("test", {refreshInterval: 0.01, holdBlocks: 1})
        await source.set(DUMMY_ROWS)
        expect(source.isCached(1000)).toEqual(true)
        await delay(50)
        expect(source.isCached(1000)).toEqual(false)
        expect(source.isCached(1)).toEqual(true)
        source.destroy()
    })
    it("should uncache blocks after a compact", async function () {
        let source = new DataSource("test", 0.01, 1)
        await source.set(DUMMY_ROWS)
        await delay(50)
        expect(source.isCached(1)).toEqual(true)
        await source.compact()
        expect(source.isCached(1)).toEqual(false)
        source.destroy()
    })
    it("should allow us to configure a larger block than we supply", async function () {
        let source = new DataSource("Test")
        source.on("retrieve", async function (params) {
            await delay(10)
            let {rowCount, startRow, rows} = params
            for (let i = startRow; i < rowCount; i++) {
                rows.push({_id: i})
            }
        })
        await source.configure({rowCount: 100})
        let row = await source.get(1)
        expect(row._id).toEqual(1)
        source.destroy()
    })
    it("should allow us to supply initial rows", async function () {
        let source = new DataSource("Test")
        let cb = jest.fn()
        source.on("retrieve", cb)
        let rows = []
        for (let i = 0; i < 256; i++) {
            rows[i] = {_id: i}
        }
        await source.configure({rowCount: 1000, rows})
        let row = await source.get(1)
        expect(row._id).toEqual(1)
        expect(cb.mock.calls.length).toEqual(0)
    })
    it("should not allow us to specify the wrong number of rows", async function () {
        let source = new DataSource("Test")
        let rows = []
        for (let i = 0; i < 254; i++) {
            rows[i] = {_id: i}
        }
        await expect(source.configure({
            rowCount: 1000,
            rows
        })).rejects.toThrow("Invalid number of rows")
        await source.configure({rowCount: 254, rows})
    })
    it("should take a number of rows as all of the rows", async function () {
        let source = new DataSource("Test")
        let rows = []
        for (let i = 0; i < 254; i++) {
            rows[i] = {_id: i}
        }
        await source.configure({rows})
    })
    it("should work without rows", async function () {
        let source = new DataSource("Test")
        await source.configure()
    })
    it("should be able to find rows", async function () {
        let source = new DataSource("Test")
        await source.set(DUMMY_ROWS)
        let items = await source.find({a: {$gte: 100}})
        expect(items.length).toBeGreaterThan(10)
    })
    it("should be able to just limit to a few rows", async function () {
        let source = new DataSource("Test")
        await source.set(DUMMY_ROWS)
        let items = await source.find({a: {$gte: 100}}, 10)
        expect(items.length).toBeLessThan(10)
    })
    it("should get all records", async function () {
        let source = new DataSource("Test")
        await source.set(DUMMY_ROWS)
        let items = await source.all()
        expect(items.length).toEqual(1000)
        items = await source.all(10)
        expect(items.length).toEqual(10)
    })
    it("should not be able to create two data sources with the same name", function () {
        new DataSource("Test")
        expect(() => {
            new DataSource("Test")
        }).toThrow("data source with that name already exists")
    })
    it("should not retrieve deleted rows", async function () {
        let source = DataSource.get("test")
        let rows = SAMPLE_ROWS()
        rows[1]._deleted = true
        await source.set(rows)
        let result = await source.all()
        expect(result.length).toEqual(3)
        result = await source.find({a: {$gt: 5}})
        expect(result.length).toEqual(1)
    })
    it("should allow us to append a row", async function () {
        let source = DataSource.get("test")
        await source.set(DUMMY_ROWS)
        await expect(source.append()).rejects.toThrow("Must supply data to append")
        await expect(source.append({a: 1})).rejects.toThrow("must have an id")
        await expect(source.append({a: 1, _id: 1})).rejects.toThrow("id must be a string")
        await expect(source.append({a: 1, _id: "1"})).rejects.toThrow("must have a type")
        await expect(source.append({
            a: 1,
            _id: "1:test"
        })).rejects.toThrow("must not already exist")
        await expect(source.append({a: 1, _id: "1001:test"})).resolves
    })
    it("should be able to stream data", async function () {
        let source = DataSource.get("test")
        await source.set(DUMMY_ROWS)
        let next = source.stream({a: {$gt: 100}}, 5)
        let counter = 0
        for (let scan = await next(); scan; scan = await next()) {
            counter ++
            expect(scan.a).toBeGreaterThan(100)
        }
        expect(counter).toBeGreaterThan(5)
    })

})
