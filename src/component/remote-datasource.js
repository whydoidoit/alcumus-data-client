import { DataSource } from './datasource';
import { dataSources, ready } from './base-data-source';
import { events } from 'alcumus-app-datasource-mutations';
import { stringify } from 'querystring';
import isObject from 'lodash/isObject';
import mingo from 'mingo';
import { EventEmitter2 } from 'eventemitter2';

let fetchApi = window.fetch;
let EventSourceApi = window.EventSource;

export function setFetchImplementation(fetchImplementation) {
    fetchApi = fetchImplementation;
}

export function setEventSourceImplementation(EventSourceImplementation) {
    EventSourceApi = EventSourceImplementation;
}

const remoteEvents = new EventEmitter2({ wildcard: true, maxListeners: 0 });

//Wire the remote mutate event into the datasource-mutations mutated event
remoteEvents.on('mutate', function (mutation) {
    events.emit('mutated', { mutation });
});

const databaseTrackers = {};

function trackDatabase (url, database) {
    let lastId = localStorage.getItem(`event-id-${database}`);
    let eventSource = new EventSourceApi(`${url}/updates/${database}${lastId ? '?lastEventId=' + lastId : ''}`);
    eventSource.addEventListener('message', function (message) {
        if (message.lastEventId) {
            localStorage.setItem(`event-id-${database}`, message.lastEventId);
        }
        let packet = JSON.parse(message.data);
        remoteEvents.emit(packet.event, packet.message);
    });
}

export class RemoteDataSource extends DataSource {
    #resolve = null;
    #reject = null;
    #promise = null;
    #url = null;
    #source = null;
    #cacheAll = false;
    #query = undefined;

    static events () {
        return remoteEvents;
    }

    static get (name, url, source, options) {
        return dataSources[name] || new RemoteDataSource(name, url, source, options);
    }

    constructor (name, url, source, { refresh = false, cacheAll = true, query, ...props } = {}) {
        if (!name) throw new Error('Must have a name');
        if (!url) throw new Error('You must supply a url');
        if (!source || source.split('/').length !== 2) throw new Error('Source must be <database>/<table>');
        super(name, props);

        let [database] = source.split('/');
        databaseTrackers[database] = databaseTrackers[database] || trackDatabase(url, database);

        if (isObject(query)) query = JSON.stringify(query);
        this.#promise = new Promise((resolve, reject) => {
            this.#resolve = resolve;
            this.#reject = reject;
        });

        this.#url = url;
        this.#source = source;
        this.#cacheAll = cacheAll;
        this.#query = query;

        const configure = async () => {
            await this[ready];
            let settings = this.settings;
            settings.url = url;
            settings.source = source;
            settings.refresh = refresh;
            settings.cacheAll = cacheAll;
            settings.query = query;
            this.settings = settings;

            this.on('retrieve', this.retrieveBlock = this.retrieveBlock.bind(this));
            remoteEvents.on('refresh-data', this.refreshData = this.refreshData.bind(this));
            remoteEvents.on('remove', function (id) {
                this.eraseRow(id).catch(console.error);
            });
            if (!settings.retrieved || refresh) {
                await this.refresh();
            } else {
                this.#resolve()
            }
            return `Configured ${source}@${url}`;
        };
        configure().then(() => {
        }, console.error);
    }

    refreshData (message) {
        try {
            if (message === 'all' || message === this.#source || message === this.name) {
                return this.refresh();
            }
        } catch (e) {

        }
    }

    destroy (withoutClearing = false) {
        this.off('retrieve', this.retrieveBlock);
        remoteEvents.off('refresh-data', this.refreshData);
        return super.destroy(withoutClearing);
    }

    async get (rowOrId) {
        await this.initialized;
        return await super.get(rowOrId);
    }

    get initialized () {
        return this.#promise;
    }

    async ready () {
        await this[ready];
        await this.initialized;
    }

    shouldContain (type, mutation) {
        let canContain = super.shouldContain(type) || type === this.#source;
        if (canContain && mutation.type === 'create') {
            return !this.#query || (this._query = this._query || new mingo.Query(JSON.parse(this.#query))).test(mutation.state);
        }
        else {
            return false;
        }
    }

    getSources (list, type) {
        super.getSources(list, type);
        if (type === this.#source) list.push(this);
    }

    async retrieveBlock ({ startRow, blockSize, rows }) {
        let settings = this.settings;
        let query = Object.assign({}, { skip: startRow, take: blockSize });
        if (settings.query) query.query = settings.query;
        let response = await fetchApi(`${this.#url}/data/${settings.source}?${stringify(query)}`, {
            credentials: 'include',
            cache: 'no-cache',
        });
        if (response.ok) {
            let result = await response.json();
            Array.prototype.push.apply(rows, result.rows);
        }
        else {
            throw new Error('Unable to retrieve block');
        }
    }

    async getMissingRecord (id) {
        try {
            let response = await fetchApi(`${this.#url}/data/${this.settings.source}?${stringify({
                query: {
                    _id: id
                },
                take: 1
            })}`, {
                credentials: 'include',
                cache: 'no-cache',
            });
            if (response && response.ok) {
                let records = await response.json();
                return records[0];
            }
            return null;
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    async refresh () {
        await this[ready];
        let settings = this.settings;
        if (!settings.source && !this.#source) {
            console.warn('Source is empty');
            this.#resolve();
            return;
        }
        let numberOfRows = settings.block_size * settings.holdBlocks;
        let query = Object.assign({});
        if (settings.query) query.query = settings.query;
        if (!settings.cacheAll) query.take = numberOfRows;

        let response = await fetchApi(`${this.#url}/data/${settings.source}?${stringify(query)}`, {
            credentials: 'include',
            cache: 'no-cache',
        });

        if (response && response.ok) {
            await this.configure(await response.json());
            settings = this.settings;
            settings.retrieved = true;
            this.settings = settings;
            this.#resolve();
        }
        else {
            console.log('Failed', response);
            this.#reject(`Failed to retrieve ${this.name} from ${this.#url} (${this.#source})`);
            throw new Error(`Failed to retrieve ${this.name} from ${this.#url} (${this.#source})`);
        }
        this.emit('refreshed');
        this.emit('updated');
    }

}

export default RemoteDataSource;
