import {applyMutation, events} from "alcumus-app-datasource-mutations"
import uniq from "lodash/uniq"

events.on("mutated", async function ({mutation}) {
    let copies = []
    const [,type] = mutation._id.split(':')
    let list = []
    await events.emitAsync(`data-source.${type}`, list, type)
    await events.emitAsync(`post.data-source.${type}`, list, type)
    await events.emitAsync(`get.${mutation._id}`, copies, mutation)

    uniq(copies).forEach(copy => {
        try {
            applyMutation(copy, mutation, false)
        } catch (e) {
            console.error(e)
        }
    })
    events.emit(`updated.${mutation._id}`)
})

export function track(item, callback) {
    let fn = items => {
        items.push(item)
    }
    events.on(`get.${item._id}`, fn)
    callback && events.on(`updated.${item._id}`, callback)
    return () => {
        events.removeListener(`get.${item._id}`, fn)
        callback && events.removeListener(`updated.${item._id}`, callback)
    }
}
