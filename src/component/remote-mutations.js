import {
    clear,
    events,
    mutations as globalMutations
} from "alcumus-app-datasource-mutations"
import debounce from "lodash/debounce"
import isEmpty from "lodash/isEmpty"
import {url as defaultUrl} from "./urls"
import storage from "localforage"

let url = defaultUrl

let delay = 1

window.addEventListener("online", processMessages)
window.addEventListener("activate", processMessages)

function processMessages() {
    delay = 1
    if (!isEmpty(globalMutations)) process()
}

export async function boot() {
    let mutations = await storage.getItem("_stored_mutations")
    if (!mutations || isEmpty(mutations)) return "NO STORED MUTATIONS"
    Object.entries(mutations).forEach(([key, mutation]) => {
        globalMutations[key] = [...mutation, ...(globalMutations[key] || [])]
    })
    process()
    return "QUEUED STORED MUTATIONS"
}

let storing = null

const processOffline = debounce(async function processOffline({mutations}) {
    if (navigator.onLine === false) {
        await Promise.resolve(storing)
        storing = storage.setItem("_stored_mutations", mutations)
    }
}, 150)

const process = debounce(async function process() {
    let mutations = globalMutations
    try {
        await storage.setItem("_stored_mutations", mutations)
        if (navigator.onLine) {
            clear()
            if (!isEmpty(mutations)) {
                await fetch(`${url}/mutate`, {
                    method: "POST",
                    mode: "cors",
                    cache: "no-cache",
                    credentials: "include",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(mutations)
                })
            }
            delay = 1
            await storage.removeItem("_stored_mutations")
        }
    } catch (e) {
        console.error(e.stack)
        //If we have an error we need to recombine
        Object.entries(mutations).forEach(([key, mutation]) => {
            globalMutations[key] = [...mutation, ...(globalMutations[key] || [])]
        })
        setTimeout(process, (delay = Math.min(30, delay + 1)) * 1000)
    }
}, 750, {maxWait: 15000})

let enabled = false


export function enable(useUrl = defaultUrl) {
    url = useUrl
    if (enabled) return
    enabled = true
    events.on("mutated", process)
    events.on("mutated", processOffline)
}

export function disable() {
    if (!enabled) return
    enabled = false
    events.removeListener("mutated", process)
    events.removeListener("mutated", processOffline)
    module.exports.flush()
}

export function flush() {
    process.flush()
}

boot().then(console.log)

