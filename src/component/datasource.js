import { generate } from 'shortid';
import map from 'lodash/map';
import isString from 'lodash/isString';
import mingo from 'mingo';
import flatten from 'lodash/flatten';
import debounce from 'lodash/debounce';
import { events } from 'alcumus-app-datasource-mutations';

import { adaptor as forageAdaptor } from './forage-adaptor';
import BaseDataSource, { dataSources, ready } from './base-data-source';

const append = Symbol('append');
const setRow = Symbol('setRow');
const write = Symbol('write');
const refreshed = Symbol('refreshed');

let _running = 0;

export class DataSource extends BaseDataSource {
    #block_size = 256;
    #settings = null;
    #blocks = {};
    #index = {};
    #timer = 0;

    adaptor = forageAdaptor;

    static get (name, refreshInterval, holdBlocks) {
        return dataSources[name] || new DataSource(name, refreshInterval, holdBlocks);
    }

    static async getSourcesFor (type) {
        if (_running > 1) return [];
        _running++;
        try {
            let list = [];
            await events.emitAsync(`data-source.${type}`, list, type);
            await events.emitAsync(`post.data-source.${type}`, list, type);
            return list;
        } finally {
            _running--;
        }
    }

    constructor (name, { refreshInterval = 60, holdBlocks = 2, adaptor } = {}) {
        super(name);
        this.adaptor = adaptor || forageAdaptor;
        this[write] = debounce(() => this.writeBlocks());
        this[refreshed] = debounce(() => this.emit('refreshed'), 5);
        this[ready] = this.settings.then((settings) => {
            settings.block_size = this.#block_size = settings.block_size || 256;
            settings.holdBlocks = holdBlocks;
            this.settings = !this.settings.then ? Object.assign(this.settings, settings) : settings;
            return this.adaptor.getIndex(this.name).then((index = {}) => {
                this.#index = index || {};
            });
        });
        this.#timer = setInterval(async () => {
            await this.cleanBlocks(holdBlocks, refreshInterval);
        }, refreshInterval * 1000);
        events.on(`get.*`, this.retrieveItemForMutation = this.retrieveItemForMutation.bind(this));
        events.on(`data-source.*`, this.getSources = this.getSources.bind(this));
    }

    get rowCount () {
        return this.settings.rowCount;
    }

    async ready () {
        return await this[ready];
    }

    getSources (list, type) {
        if (type === this.name) list.push(this);
    }

    shouldContain (type) {
        return this.name === type;
    }

    async getMissingRecord (id) {
    }

    async retrieveItemForMutation (list, mutation) {
        const storeData = () => {
            item && this[setRow](this.#index[mutation._id], item, false);
        };
        let [, type] = mutation._id.split(':');
        let item;
        try {
            item = await this.get(mutation._id);
            list.push(item);
            events.once(`updated.${item._id}`, storeData);
        } catch (e) {
            //Add it to the collection if we are creating a new item
            if (e.message === 'Invalid row' && mutation.type === 'create' && this.shouldContain(type, mutation)) {
                item = { _id: mutation._id };
                list.push(item);
                events.once(`updated.${item._id}`, storeData);
                await this.append(item);
            }
            else if (e.message === 'Invalid row') {
                let item = await this.getMissingRecord(mutation._id);
                if (item) {
                    list.push(item);
                    events.once(`updated.${item._id}`, storeData);
                    await this.append(item);
                }
            }

        }

    }

    async cleanBlocks (holdBlocks = 0, refreshInterval = 0) {
        await Promise.all(Object.entries(this.#blocks).map(async ([key, blockPromise]) => {
            let block = await blockPromise;
            if (!block.updated && key >= holdBlocks && Date.now() - block.time > refreshInterval * 1000) {
                delete this.#blocks[key];
            }
        }));
    }

    destroy (withoutClearing = false) {
        super.destroy();
        events.off('get.*', this.retrieveItemForMutation);
        events.off('data-source.*', this.getSources);
        clearInterval(this.#timer);
        return withoutClearing ? Promise.resolve(withoutClearing) : this.clear();
    }

    isCached (row) {
        let blockNumber = Math.floor(row / this.#block_size);
        return !!this.#blocks[blockNumber];
    }

    get settings () {
        return this.#settings = this.#settings ||
            this.adaptor.getSettings(this.name).then(result => result || {
                block_size: 0,
                rowCount: 0
            });
    }

    set settings (value) {
        this.#settings = value;
        return this.adaptor.setSettings(name, value);
    }

    async clear () {
        let { block_size, rowCount } = this.settings;
        for (let b = 0; b < rowCount / block_size + 1; b++) {
            await this.adaptor.removeBlock(name, b);
        }
        this.settings = Object.assign(this.settings, {
            block_size: this.#block_size,
            rowCount: 0
        });
        this.#blocks = {};
        this.#index = {};
        await this.adaptor.setIndex(name, {});
    }

    async getBlockForRow (row, isNew = false) {
        let blockNumber = Math.floor(row / this.#block_size);

        let blockPromise = this.#blocks[blockNumber];
        if (!blockPromise) {
            blockPromise = this.#blocks[blockNumber] = this.adaptor.getBlock(name, blockNumber).then(async rows => {
                if (!rows) {
                    let settings = this.settings;
                    let params = {
                        blockNumber,
                        startRow: blockNumber * this.#block_size,
                        blockSize: this.#block_size,
                        rowCount: settings.rowCount,
                        rows: []
                    };

                    !isNew && await this.emitAsync('retrieve', params);
                    rows = params.rows;
                }
                return { rows, updated: false, time: 0 };
            });
        }
        let block = await blockPromise;
        block.time = Date.now();
        return { block: block, offset: row % this.#block_size };
    }

    async writeBlocks () {
        await Promise.all(map(this.#blocks, async (blockPromise, key) => {
            let block = await blockPromise;
            if (block.updated) {
                await this.adaptor.setBlock(name, key, block.rows);
                block.updated = false;
            }
        }));
    }

    async [setRow] (row, data, isNew) {
        let { block, offset } = await this.getBlockForRow(row, isNew);
        block.rows[offset] = data;
        block.updated = true;
        data._id = data._id || generate();
        this.#index[data._id] = row;
        this[write]();
        this[refreshed]();
    }

    async eraseRow (row) {
        let rowNum = this.getRowNumber(row);
        if (rowNum !== undefined) {
            this[setRow](rowNum, { _id: 'erased', _deleted: true }, false);
            delete this.#index[row];
            this[write]();
            this[refreshed]();
        }
    }

    async [append] (data) {
        if (Array.isArray(data)) {
            await Promise.all(data.map(async row => {
                await this[append](row);
            }));
        }
        else {
            let settings = this.settings;
            let row = settings.rowCount++;
            this.settings = settings;
            await this[setRow](row, data, true);
        }
    }

    async append (row) {
        if (!row) throw new Error('Must supply data to append');
        if (!row._id) throw new Error('Row must have an id');
        if (!isString(row._id)) throw new Error('Row id must be a string');
        if (-1 === row._id.indexOf(':')) throw new Error('Row id must have a type');
        await this.ready();
        if (this.#index[row._id]) throw new Error('Row must not already exist');
        return this[append](row);
    }

    async get (rowNumberOrId) {
        if (rowNumberOrId === undefined) {
            throw new Error('Invalid row');
        }
        if (isString(rowNumberOrId)) {
            await this.ready();
            return this.get(this.#index[rowNumberOrId]);
        }
        else {
            let { block, offset } = await this.getBlockForRow(rowNumberOrId);
            return block.rows[offset];
        }
    }

    getRowNumber (id) {
        return this.#index[id];
    }

    async set (rows) {
        await this.clear();
        return this[append](rows);
    }

    async configure (options = {}) {
        await this.clear();
        options.rowCount = options.rowCount || (options.rows || []).length;

        this.settings = Object.assign(this.settings, {
            block_size: options.blockSize || 256,
            rowCount: options.rowCount || 0
        });
        if (options.rows) {
            if (options.rows.length !== options.rowCount && options.rows.length < this.#block_size) {
                throw new Error('Invalid number of rows, must be all rows or more than a block');
            }
            options.rows.length = Math.max(options.rowCount, Math.floor(options.rows.length / this.#block_size) * this.#block_size);
            await Promise.all(options.rows.map(async (row, index) => {
                return this[setRow](index, row, true);
            }));
        }
        this[refreshed]();
    }

    async find (query, limit, start = 0, includeDeleted = false) {
        let qry = new mingo.Query(query);
        let rows = [];
        const { rowCount, block_size } = this.settings;
        limit = Math.min(limit || rowCount, rowCount);
        let remaining = limit;
        for (let row = start; row < limit; row += block_size, remaining -= block_size) {
            let { block } = await this.getBlockForRow(row);
            let cursor = qry.find(block.rows.slice(0, remaining).filter(item => !item._deleted || includeDeleted));
            rows.push(cursor.all());
        }
        return flatten(rows);
    }

    stream (query, start = 0, includeDeleted = false) {
        let row = start;
        const qry = new mingo.Query(query);
        const settings = this.settings;
        return async () => {
            try {
                let item;
                do {
                    item = await this.get(row++);
                    if (item && item._deleted && !includeDeleted) item = null;
                    if (item && !qry.test(item)) item = null;
                    if (item) return item;
                } while (!item && row < settings.rowCount);
                return null;
            } finally {
                await this.cleanBlocks(this.settings.holdBlocks || 2, 10);
            }
        };
    }

    async compact () {
        await this.writeBlocks();
        await this.cleanBlocks(0, -10);
    }

    async all (limit, start = 0, includeDeleted = false) {
        let rows = [];
        await this.ready();
        const { rowCount, block_size } = this.settings;
        limit = Math.min(limit || rowCount, rowCount);
        let remaining = limit;
        for (let row = start; row < limit; row += block_size, remaining -= block_size) {
            let { block } = await this.getBlockForRow(row);
            rows.push(block.rows.slice(0, remaining).filter(item => !item._deleted || includeDeleted));
        }
        return flatten(rows);
    }
}

export default DataSource;
