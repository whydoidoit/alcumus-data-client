import DataSource from "./datasource"
import {assign, create, events, set} from "alcumus-app-datasource-mutations"
import {track} from "./datasource-mutations"
import "alcumus-app-datasource-mutations/dist/stable-conflict-resolution"

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}

//Allow tests to create new data sources
events.on("post.data-source.*", async function (list, type) {
    if (list.length !== 0) return
    list.push(new DataSource(type))
})

describe("Mutation reactions", function () {

    it("should create an entry for a new item and track changes on it", async function () {
        let item = create("test", {a: 1})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
        assign(item, {b: 2, c: 3})
        await delay()
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
    })
    it("should have changes persisted to permanent storage", async function () {
        let item = create("test", {a: 1})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
        assign(item, {b: 2, c: 3})
        await delay()
        await DataSource.byName.test.compact()
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
    })
    it("should not create a new entry for a modification", async function () {
        let item = set("test", "1", {a: 1, _id: "123:test"})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        await expect(DataSource.byName.test.get(item._id)).rejects.toThrow("Invalid row")
    })
    it("should be able to keep mirror items", async function () {
        let item = create("test", {a: 1})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        item = JSON.parse(JSON.stringify(item))
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
        item = JSON.parse(JSON.stringify(item))
        assign(item, {b: 2, c: 3})
        await delay()
        await expect(DataSource.byName.test.get(item._id)).resolves.toEqual(item)
    })
    it("should update the in memory copy of an item", async function () {
        let item = create("test", {a: 1})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        let test = await DataSource.byName.test.get(item._id)
        assign(item, {b: 2, c: 3})
        await delay()
        expect(test).toEqual(item)
        expect(test !== item)
        assign(item, {d: 4})
        await delay()
        expect(test).toEqual(item)
    })
    it("should allow us to create an event updated copy of an item", async function () {
        let copyItem = {_id: "1:test"}
        let cancel = track(copyItem)
        let item = create("test", {a: 1, _id: "1:test"})
        await delay()
        expect(copyItem).toEqual(item)
        assign(item, {d: 4})
        await delay()
        expect(copyItem).toEqual(item)
        cancel()
        assign(item, {e: 5})
        await delay()
        expect(copyItem).not.toEqual(item)
    })
    it("should fire a callback event when a tracked object is changed", async function () {
        let cb = jest.fn()
        let copyItem = {_id: "1:test"}
        let cancel = track(copyItem, cb)
        let item = create("test", {a: 1, _id: "1:test"})
        await delay()
        expect(copyItem).toEqual(item)
        assign(item, {d: 4})
        await delay()
        expect(cb.mock.calls.length).toEqual(2)
        cancel()
        expect(copyItem).toEqual(item)
        assign(item, {e: 5})
        await delay()
        expect(cb.mock.calls.length).toEqual(2)
    })
    it("should not fail if tracking a retrieved item within its timeout", async function () {
        let item = create("test", {a: 1})
        await delay()
        expect(DataSource.byName.test).not.toEqual(undefined)
        let test = await DataSource.byName.test.get(item._id)
        expect(test !== item).toEqual(true)
        let cancel = track(test)
        assign(item, {b: 2, c: 3})
        await delay()
        expect(test).toEqual(item)
        cancel()
        assign(item, {e: 5})
        await delay(20)
        expect(test).toEqual(item)
    })
})
