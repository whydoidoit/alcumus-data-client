import {create, mutations, set, clear} from "alcumus-app-datasource-mutations"
import {enable, disable, boot} from "./remote-mutations"
import storage from "localforage"

const FetchMock = require("jest-fetch-mock")
const realFetch = global.fetch

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}


describe("Remote mutations", function() {
    beforeAll(async ()=>{
        global.fetch = FetchMock
    })
    afterAll(()=>{
        global.fetch = realFetch
    })
    beforeEach(async ()=>{
        await storage.removeItem("_stored_mutations")
        fetch.resetMocks()
        enable()
    })
    afterEach(()=>{
        disable()
    })
    it("should store mutations if the call fails", async function() {
        fetch.mockReject(new Error("Could not reach server"))
        let created = create("database/test", {hello: 1})
        await delay(800)
        let item = await storage.getItem("_stored_mutations")
        expect(item[created._id][0]._id).toEqual(created._id)
    })
    it("should not store mutations if the call succeeds", async function () {
        fetch.mockResponseOnce(JSON.stringify({ok: true}))
        create("database/test", {hello: 1})
        await delay(800)
        let item = await storage.getItem("_stored_mutations")
        expect(item).toEqual(null)
    })
    it("should boot up with mutations", async function () {
        fetch.mockReject(new Error("Could not reach server"))
        let created = create("database/test", {hello: 1})
        await delay(800)
        disable()
        clear()
        await enable()
        await boot()
        expect(mutations[created._id].length).toEqual(1)
    })
    it("should combine mutations on a failure", async function () {
        fetch.mockReject(new Error("Could not reach server"))
        let created = create("database/test", {hello: 1})
        await delay(800)
        set("test", 1, created)
        await delay(800)
        expect(mutations[created._id].length).toEqual(2)
    })
})
