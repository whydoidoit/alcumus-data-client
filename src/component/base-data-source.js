import { EventEmitter2 } from 'eventemitter2';

export const ready = Symbol('ready');

export const dataSources = {};

export class BaseDataSource extends EventEmitter2 {

    static get byName () {
        return dataSources;
    }

    static get (name) {
        let result = dataSources[name] || null;
        if(!result) throw new Error('Nothing to create');
        return result;
    }

    constructor (name) {
        super({ maxListeners: 100, wildcard: true });
        if (dataSources[name]) {
            console.log(Object.keys(dataSources))
            throw new Error("A data source with that name already exists")
        }
        dataSources[name] = this
        this.name = name;
        dataSources[name] = this;
        this[ready] = Promise.resolve(true);
    }

    get rowCount () {
        return this.settings.rowCount;
    }

    async ready () {
        return await this[ready];
    }

    getSources (list, type) {
        if (type === this.name) list.push(this);
    }

    destroy () {
        delete dataSources[this.name];
    }

    async get (rowNumberOrId) {
        if (rowNumberOrId === undefined) {
            throw new Error('Invalid row');
        }
        throw new Error('Not found');
    }

    async find (query, limit, start, includeDeleted = false) {
        return [];
    }

    stream (query, start = 0, includeDeleted = false) {
        //Return an async function that will return a record or null if no more match
        return async () => {
        };
    }

    async all (limit, start, includeDeleted = false) {
        return [];
    }
}

export default BaseDataSource;
