import storage from "localforage"

export const adaptor = {
    async getBlock(name, block) {
        return await storage.getItem(`block-${name}-${block}`)
    },
    async setBlock(name, block, data) {
        return await storage.setItem(`block-${name}-${block}`, data)
    },
    async removeBlock(name, block) {
        return await storage.removeItem(`block-${name}-${block}`)
    },
    async getSettings(name) {
        return await storage.getItem(`settings-${name}`)
    },
    async setSettings(name, value) {
        return await storage.setItem(`settings-${name}`, value)
    },
    async getIndex(name) {
        return await storage.getItem(`index-${name}`)
    },
    async setIndex(name, value) {
        return await storage.setItem(`index-${name}`, value)
    }
}

export default adaptor
