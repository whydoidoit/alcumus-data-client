import EventSource, { sources } from "eventsourcemock"
import DataSource from "./datasource"
import {
    RemoteDataSource,
    setEventSourceImplementation,
    setFetchImplementation
} from './remote-datasource';
import "./datasource-mutations"
import {set} from "alcumus-app-datasource-mutations"

global.EventSource = EventSource
setEventSourceImplementation(EventSource);

const DUMMY_ROWS = []
for (let i = 0; i < 1000; i++) {
    DUMMY_ROWS.push({_id: `${i}:database/test`, a: Math.floor(Math.random() * 200)})
}

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}


describe("Remote data source", function () {
    afterEach(async () => {
        try {
            await Promise.all(Object.entries(DataSource.byName).map(async ([key, dataSource]) => {
                try {
                    try {
                        await dataSource.initialized
                    } catch (e) {

                    }
                    await dataSource.destroy()
                } catch(e) {

                }
            }))
        } catch(e) {
            console.log(e)
        }
    })
    beforeEach(async () => {
        global.fetch = require("jest-fetch-mock")
        setFetchImplementation(global.fetch);
        await fetch.resetMocks()
    })
    it("should be able to get sources for both name and source", async function () {
        let source = new RemoteDataSource("hello", "/", "database/test")
        await source.ready
        let list = []
        source.getSources(list, "hello")
        expect(list.length).toEqual(1)
        list.length = 0
        source.getSources(list, "database/test")
        expect(list.length).toEqual(1)
        list.length = 0
        source.getSources(list, "banana/test")
        expect(list.length).toEqual(0)
    })
    it("should fail to create a remote data source if the name or url is wrong", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(0, 511),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        expect(() => new RemoteDataSource()).toThrow("Must have a name")
        expect(() => new RemoteDataSource("test1")).toThrow("must supply a url")
        expect(() => new RemoteDataSource("test2", "/")).toThrow("Source must be")
        expect(() => new RemoteDataSource("test3", "/", "hello")).toThrow("Source must be")
        expect(() => new RemoteDataSource("test4", "/", "hello/mum/how")).toThrow("Source must be")
        expect(() => new RemoteDataSource("test5", "/", "hello/mum")).not.toThrow()
    })
    it("should report errors if unable to retrieve", async function () {
        fetch.mockResponse({}, {ok: false, status: 500})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await expect(source.initialized).rejects.toMatch(/Failed to retrieve/)
    })
    it("should be able to retrieve data sources", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(0, 511),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await expect(DataSource.getSourcesFor("testRM")).resolves.toEqual(expect.objectContaining({length: 1}))
        await expect(DataSource.getSourcesFor("database/test")).resolves.toEqual(expect.objectContaining({length: 1}))
        await expect(DataSource.getSourcesFor("banana/test")).resolves.toEqual(expect.objectContaining({length: 0}))
    })

    it("should be able to retrieve", async function () {
        await delay()
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS,
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await source.initialized
        // expect(fetch).not.toHaveBeenLastCalledWith(expect.stringContaining("take="), expect.any(Object))
        // expect(fetch).not.toHaveBeenLastCalledWith(expect.stringContaining("query="), expect.any(Object))
        let row = await source.get(2)
        expect(row._id).toEqual("2:database/test")

    })
    it("should be able to retrieve additional blocks", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(0, 511),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test", {cacheAll: false})
        await source.initialized
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(768),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let row = await source.get(999)
        expect(fetch).toHaveBeenLastCalledWith(expect.stringContaining("skip=768"), expect.any(Object))
        expect(row._id).toEqual("999:database/test")

    })
    it("should be able to handle a failure to retrieve additional blocks", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(0, 511),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await source.initialized
        fetch.mockResponse(JSON.stringify({}), {ok: false, status: 500})
        await expect(source.get(999)).rejects.toThrow("Unable to retrieve block")

    })
    it("should pass on a query to the target", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(0, 511),
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test", {query: {a: {$gte: 100}}})
        await source.initialized
        expect(fetch).toHaveBeenLastCalledWith(expect.stringContaining("query=%7B"), expect.any(Object))

    })
    it("should create an item from events", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS,
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await source.initialized
        const event = new MessageEvent("message", {
            type: "message", data: JSON.stringify({
                event: "mutate",
                message: {
                    type: 'create',
                    itemType: "database/test",
                    _id: "x1:database/test",
                    state: {a: 100, hello: 'world', _id: "x1:database/test"}
                }
            })
        })
        sources['//updates/database'].emit(event.type, event)
        await delay(200)
        expect(source.rowCount).toEqual(1001)

    })
    it("should not create an item from events if it mismatches the query", async function () {
        await delay()
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS,
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test", {query: {a: {$gt: 101}}})
        await source.initialized
        const event = new MessageEvent("message", {
            type: "message", data: JSON.stringify({
                event: "mutate",
                message: {
                    type: 'create',
                    itemType: "database/test",
                    _id: "x1:database/test",
                    state: {a: 100, hello: 'world', _id: "x1:database/test"}
                }
            })
        })
        sources['//updates/database'].emit(event.type, event)
        await delay()
        expect(source.rowCount).toEqual(1000)
    })
    it("should update an item from an event", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS,
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await source.initialized
        let result = set("hello", "world")
        result._id = "1:database/test"
        const event = new MessageEvent("message", {
            type: "message",
            data: JSON.stringify({event: "mutate", message: result}),
            lastEventId: 1
        })
        sources['//updates/database'].emit(event.type, event)
        await delay()
        let row = await source.get(1)
        expect(row.hello).toEqual("world")
    })
    it("should refresh from an event", async function () {
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS,
            rowCount: DUMMY_ROWS.length
        }), {ok: true, status: 200})
        let source = new RemoteDataSource("testRM", "/", "database/test")
        await source.initialized
        let event = new MessageEvent("message", {
            type: "message",
            data: JSON.stringify({event: "refresh-data", message: "all"})
        })
        expect(source.rowCount).toEqual(1000)

        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(500),
            rowCount: 500
        }), {ok: true, status: 200})
        sources['//updates/database'].emit(event.type, event)
        await delay(250)
        expect(source.rowCount).toEqual(500)
        event = new MessageEvent("message", {
            data: JSON.stringify({event: "refresh-data", message: "testRM"})
        })
        fetch.mockResponse(JSON.stringify({
            rows: DUMMY_ROWS.slice(50),
            rowCount: 50
        }), {ok: true, status: 200})
        sources['//updates/database'].emit(event.type, event)
        await delay(150)
        expect(source.rowCount).toEqual(50)
    })
})
