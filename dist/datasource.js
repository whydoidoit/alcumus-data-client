"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.DataSource = void 0;

var _shortid = require("shortid");

var _map = _interopRequireDefault(require("lodash/map"));

var _isString = _interopRequireDefault(require("lodash/isString"));

var _mingo = _interopRequireDefault(require("mingo"));

var _flatten = _interopRequireDefault(require("lodash/flatten"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _alcumusAppDatasourceMutations = require("alcumus-app-datasource-mutations");

var _forageAdaptor = require("./forage-adaptor");

var _baseDataSource = _interopRequireWildcard(require("./base-data-source"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classPrivateFieldGet(receiver, privateMap) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return privateMap.get(receiver).value; }

function _classPrivateFieldSet(receiver, privateMap, value) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to set private field on non-instance"); } var descriptor = privateMap.get(receiver); if (!descriptor.writable) { throw new TypeError("attempted to set read only private field"); } descriptor.value = value; return value; }

var _append = Symbol('append');

var setRow = Symbol('setRow');
var write = Symbol('write');
var refreshed = Symbol('refreshed');
var _running = 0;

var DataSource =
/*#__PURE__*/
function (_BaseDataSource) {
  _inherits(DataSource, _BaseDataSource);

  _createClass(DataSource, null, [{
    key: "get",
    value: function get(name, refreshInterval, holdBlocks) {
      return _baseDataSource.dataSources[name] || new DataSource(name, refreshInterval, holdBlocks);
    }
  }, {
    key: "getSourcesFor",
    value: function () {
      var _getSourcesFor = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(type) {
        var list;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(_running > 1)) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return", []);

              case 2:
                _running++;
                _context.prev = 3;
                list = [];
                _context.next = 7;
                return _alcumusAppDatasourceMutations.events.emitAsync("data-source.".concat(type), list, type);

              case 7:
                _context.next = 9;
                return _alcumusAppDatasourceMutations.events.emitAsync("post.data-source.".concat(type), list, type);

              case 9:
                return _context.abrupt("return", list);

              case 10:
                _context.prev = 10;
                _running--;
                return _context.finish(10);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[3,, 10, 13]]);
      }));

      function getSourcesFor(_x) {
        return _getSourcesFor.apply(this, arguments);
      }

      return getSourcesFor;
    }()
  }]);

  function DataSource(name) {
    var _this;

    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$refreshInterval = _ref.refreshInterval,
        refreshInterval = _ref$refreshInterval === void 0 ? 60 : _ref$refreshInterval,
        _ref$holdBlocks = _ref.holdBlocks,
        holdBlocks = _ref$holdBlocks === void 0 ? 2 : _ref$holdBlocks,
        adaptor = _ref.adaptor;

    _classCallCheck(this, DataSource);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DataSource).call(this, name));

    _block_size.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: 256
    });

    _settings.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _blocks.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: {}
    });

    _index.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: {}
    });

    _timer.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: 0
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "adaptor", _forageAdaptor.adaptor);

    _this.adaptor = adaptor || _forageAdaptor.adaptor;
    _this[write] = (0, _debounce.default)(function () {
      return _this.writeBlocks();
    });
    _this[refreshed] = (0, _debounce.default)(function () {
      return _this.emit('refreshed');
    }, 5);
    _this[_baseDataSource.ready] = _this.settings.then(function (settings) {
      settings.block_size = _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _block_size, settings.block_size || 256);
      settings.holdBlocks = holdBlocks;
      _this.settings = !_this.settings.then ? Object.assign(_this.settings, settings) : settings;
      return _this.adaptor.getIndex(_this.name).then(function () {
        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _index, index || {});
      });
    });

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _timer, setInterval(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _this.cleanBlocks(holdBlocks, refreshInterval);

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    })), refreshInterval * 1000));

    _alcumusAppDatasourceMutations.events.on("get.*", _this.retrieveItemForMutation = _this.retrieveItemForMutation.bind(_assertThisInitialized(_assertThisInitialized(_this))));

    _alcumusAppDatasourceMutations.events.on("data-source.*", _this.getSources = _this.getSources.bind(_assertThisInitialized(_assertThisInitialized(_this))));

    return _this;
  }

  _createClass(DataSource, [{
    key: "ready",
    value: function () {
      var _ready2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this[_baseDataSource.ready];

              case 2:
                return _context3.abrupt("return", _context3.sent);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function ready() {
        return _ready2.apply(this, arguments);
      }

      return ready;
    }()
  }, {
    key: "getSources",
    value: function getSources(list, type) {
      if (type === this.name) list.push(this);
    }
  }, {
    key: "shouldContain",
    value: function shouldContain(type) {
      return this.name === type;
    }
  }, {
    key: "getMissingRecord",
    value: function () {
      var _getMissingRecord = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(id) {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getMissingRecord(_x2) {
        return _getMissingRecord.apply(this, arguments);
      }

      return getMissingRecord;
    }()
  }, {
    key: "retrieveItemForMutation",
    value: function () {
      var _retrieveItemForMutation = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(list, mutation) {
        var _this2 = this;

        var storeData, _mutation$_id$split, _mutation$_id$split2, type, item, _item;

        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                storeData = function storeData() {
                  item && _this2[setRow](_classPrivateFieldGet(_this2, _index)[mutation._id], item, false);
                };

                _mutation$_id$split = mutation._id.split(':'), _mutation$_id$split2 = _slicedToArray(_mutation$_id$split, 2), type = _mutation$_id$split2[1];
                _context5.prev = 2;
                _context5.next = 5;
                return this.get(mutation._id);

              case 5:
                item = _context5.sent;
                list.push(item);

                _alcumusAppDatasourceMutations.events.once("updated.".concat(item._id), storeData);

                _context5.next = 29;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](2);

                if (!(_context5.t0.message === 'Invalid row' && mutation.type === 'create' && this.shouldContain(type, mutation))) {
                  _context5.next = 20;
                  break;
                }

                item = {
                  _id: mutation._id
                };
                list.push(item);

                _alcumusAppDatasourceMutations.events.once("updated.".concat(item._id), storeData);

                _context5.next = 18;
                return this.append(item);

              case 18:
                _context5.next = 29;
                break;

              case 20:
                if (!(_context5.t0.message === 'Invalid row')) {
                  _context5.next = 29;
                  break;
                }

                _context5.next = 23;
                return this.getMissingRecord(mutation._id);

              case 23:
                _item = _context5.sent;

                if (!_item) {
                  _context5.next = 29;
                  break;
                }

                list.push(_item);

                _alcumusAppDatasourceMutations.events.once("updated.".concat(_item._id), storeData);

                _context5.next = 29;
                return this.append(_item);

              case 29:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[2, 10]]);
      }));

      function retrieveItemForMutation(_x3, _x4) {
        return _retrieveItemForMutation.apply(this, arguments);
      }

      return retrieveItemForMutation;
    }()
  }, {
    key: "cleanBlocks",
    value: function () {
      var _cleanBlocks = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7() {
        var _this3 = this;

        var holdBlocks,
            refreshInterval,
            _args7 = arguments;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                holdBlocks = _args7.length > 0 && _args7[0] !== undefined ? _args7[0] : 0;
                refreshInterval = _args7.length > 1 && _args7[1] !== undefined ? _args7[1] : 0;
                _context7.next = 4;
                return Promise.all(Object.entries(_classPrivateFieldGet(this, _blocks)).map(
                /*#__PURE__*/
                function () {
                  var _ref4 = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee6(_ref3) {
                    var _ref5, key, blockPromise, block;

                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                      while (1) {
                        switch (_context6.prev = _context6.next) {
                          case 0:
                            _ref5 = _slicedToArray(_ref3, 2), key = _ref5[0], blockPromise = _ref5[1];
                            _context6.next = 3;
                            return blockPromise;

                          case 3:
                            block = _context6.sent;

                            if (!block.updated && key >= holdBlocks && Date.now() - block.time > refreshInterval * 1000) {
                              delete _classPrivateFieldGet(_this3, _blocks)[key];
                            }

                          case 5:
                          case "end":
                            return _context6.stop();
                        }
                      }
                    }, _callee6, this);
                  }));

                  return function (_x5) {
                    return _ref4.apply(this, arguments);
                  };
                }()));

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function cleanBlocks() {
        return _cleanBlocks.apply(this, arguments);
      }

      return cleanBlocks;
    }()
  }, {
    key: "destroy",
    value: function destroy() {
      var withoutClearing = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      _get(_getPrototypeOf(DataSource.prototype), "destroy", this).call(this);

      _alcumusAppDatasourceMutations.events.off('get.*', this.retrieveItemForMutation);

      _alcumusAppDatasourceMutations.events.off('data-source.*', this.getSources);

      clearInterval(_classPrivateFieldGet(this, _timer));
      return withoutClearing ? Promise.resolve(withoutClearing) : this.clear();
    }
  }, {
    key: "isCached",
    value: function isCached(row) {
      var blockNumber = Math.floor(row / _classPrivateFieldGet(this, _block_size));
      return !!_classPrivateFieldGet(this, _blocks)[blockNumber];
    }
  }, {
    key: "clear",
    value: function () {
      var _clear = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee8() {
        var _this$settings, block_size, rowCount, b;

        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _this$settings = this.settings, block_size = _this$settings.block_size, rowCount = _this$settings.rowCount;
                b = 0;

              case 2:
                if (!(b < rowCount / block_size + 1)) {
                  _context8.next = 8;
                  break;
                }

                _context8.next = 5;
                return this.adaptor.removeBlock(name, b);

              case 5:
                b++;
                _context8.next = 2;
                break;

              case 8:
                this.settings = Object.assign(this.settings, {
                  block_size: _classPrivateFieldGet(this, _block_size),
                  rowCount: 0
                });

                _classPrivateFieldSet(this, _blocks, {});

                _classPrivateFieldSet(this, _index, {});

                _context8.next = 13;
                return this.adaptor.setIndex(name, {});

              case 13:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function clear() {
        return _clear.apply(this, arguments);
      }

      return clear;
    }()
  }, {
    key: "getBlockForRow",
    value: function () {
      var _getBlockForRow = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee10(row) {
        var _this4 = this;

        var isNew,
            blockNumber,
            blockPromise,
            block,
            _args10 = arguments;
        return regeneratorRuntime.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                isNew = _args10.length > 1 && _args10[1] !== undefined ? _args10[1] : false;
                blockNumber = Math.floor(row / _classPrivateFieldGet(this, _block_size));
                blockPromise = _classPrivateFieldGet(this, _blocks)[blockNumber];

                if (!blockPromise) {
                  blockPromise = _classPrivateFieldGet(this, _blocks)[blockNumber] = this.adaptor.getBlock(name, blockNumber).then(
                  /*#__PURE__*/
                  function () {
                    var _ref6 = _asyncToGenerator(
                    /*#__PURE__*/
                    regeneratorRuntime.mark(function _callee9(rows) {
                      var _settings2, params;

                      return regeneratorRuntime.wrap(function _callee9$(_context9) {
                        while (1) {
                          switch (_context9.prev = _context9.next) {
                            case 0:
                              if (rows) {
                                _context9.next = 8;
                                break;
                              }

                              _settings2 = _this4.settings;
                              params = {
                                blockNumber: blockNumber,
                                startRow: blockNumber * _classPrivateFieldGet(_this4, _block_size),
                                blockSize: _classPrivateFieldGet(_this4, _block_size),
                                rowCount: _settings2.rowCount,
                                rows: []
                              };
                              _context9.t0 = !isNew;

                              if (!_context9.t0) {
                                _context9.next = 7;
                                break;
                              }

                              _context9.next = 7;
                              return _this4.emitAsync('retrieve', params);

                            case 7:
                              rows = params.rows;

                            case 8:
                              return _context9.abrupt("return", {
                                rows: rows,
                                updated: false,
                                time: 0
                              });

                            case 9:
                            case "end":
                              return _context9.stop();
                          }
                        }
                      }, _callee9, this);
                    }));

                    return function (_x7) {
                      return _ref6.apply(this, arguments);
                    };
                  }());
                }

                _context10.next = 6;
                return blockPromise;

              case 6:
                block = _context10.sent;
                block.time = Date.now();
                return _context10.abrupt("return", {
                  block: block,
                  offset: row % _classPrivateFieldGet(this, _block_size)
                });

              case 9:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this);
      }));

      function getBlockForRow(_x6) {
        return _getBlockForRow.apply(this, arguments);
      }

      return getBlockForRow;
    }()
  }, {
    key: "writeBlocks",
    value: function () {
      var _writeBlocks = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee12() {
        var _this5 = this;

        return regeneratorRuntime.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.next = 2;
                return Promise.all((0, _map.default)(_classPrivateFieldGet(this, _blocks),
                /*#__PURE__*/
                function () {
                  var _ref7 = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee11(blockPromise, key) {
                    var block;
                    return regeneratorRuntime.wrap(function _callee11$(_context11) {
                      while (1) {
                        switch (_context11.prev = _context11.next) {
                          case 0:
                            _context11.next = 2;
                            return blockPromise;

                          case 2:
                            block = _context11.sent;

                            if (!block.updated) {
                              _context11.next = 7;
                              break;
                            }

                            _context11.next = 6;
                            return _this5.adaptor.setBlock(name, key, block.rows);

                          case 6:
                            block.updated = false;

                          case 7:
                          case "end":
                            return _context11.stop();
                        }
                      }
                    }, _callee11, this);
                  }));

                  return function (_x8, _x9) {
                    return _ref7.apply(this, arguments);
                  };
                }()));

              case 2:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, this);
      }));

      function writeBlocks() {
        return _writeBlocks.apply(this, arguments);
      }

      return writeBlocks;
    }()
  }, {
    key: setRow,
    value: function () {
      var _value = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee13(row, data, isNew) {
        var _ref8, block, offset;

        return regeneratorRuntime.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.next = 2;
                return this.getBlockForRow(row, isNew);

              case 2:
                _ref8 = _context13.sent;
                block = _ref8.block;
                offset = _ref8.offset;
                block.rows[offset] = data;
                block.updated = true;
                data._id = data._id || (0, _shortid.generate)();
                _classPrivateFieldGet(this, _index)[data._id] = row;
                this[write]();
                this[refreshed]();

              case 11:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, this);
      }));

      function value(_x10, _x11, _x12) {
        return _value.apply(this, arguments);
      }

      return value;
    }()
  }, {
    key: "eraseRow",
    value: function () {
      var _eraseRow = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee14(row) {
        var rowNum;
        return regeneratorRuntime.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                rowNum = this.getRowNumber(row);

                if (rowNum !== undefined) {
                  this[setRow](rowNum, {
                    _id: 'erased',
                    _deleted: true
                  }, false);
                  delete _classPrivateFieldGet(this, _index)[row];
                  this[write]();
                  this[refreshed]();
                }

              case 2:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14, this);
      }));

      function eraseRow(_x13) {
        return _eraseRow.apply(this, arguments);
      }

      return eraseRow;
    }()
  }, {
    key: _append,
    value: function () {
      var _value2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee16(data) {
        var _this6 = this;

        var _settings3, row;

        return regeneratorRuntime.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                if (!Array.isArray(data)) {
                  _context16.next = 5;
                  break;
                }

                _context16.next = 3;
                return Promise.all(data.map(
                /*#__PURE__*/
                function () {
                  var _ref9 = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee15(row) {
                    return regeneratorRuntime.wrap(function _callee15$(_context15) {
                      while (1) {
                        switch (_context15.prev = _context15.next) {
                          case 0:
                            _context15.next = 2;
                            return _this6[_append](row);

                          case 2:
                          case "end":
                            return _context15.stop();
                        }
                      }
                    }, _callee15, this);
                  }));

                  return function (_x15) {
                    return _ref9.apply(this, arguments);
                  };
                }()));

              case 3:
                _context16.next = 10;
                break;

              case 5:
                _settings3 = this.settings;
                row = _settings3.rowCount++;
                this.settings = _settings3;
                _context16.next = 10;
                return this[setRow](row, data, true);

              case 10:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16, this);
      }));

      function value(_x14) {
        return _value2.apply(this, arguments);
      }

      return value;
    }()
  }, {
    key: "append",
    value: function () {
      var _append2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee17(row) {
        return regeneratorRuntime.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                if (row) {
                  _context17.next = 2;
                  break;
                }

                throw new Error('Must supply data to append');

              case 2:
                if (row._id) {
                  _context17.next = 4;
                  break;
                }

                throw new Error('Row must have an id');

              case 4:
                if ((0, _isString.default)(row._id)) {
                  _context17.next = 6;
                  break;
                }

                throw new Error('Row id must be a string');

              case 6:
                if (!(-1 === row._id.indexOf(':'))) {
                  _context17.next = 8;
                  break;
                }

                throw new Error('Row id must have a type');

              case 8:
                _context17.next = 10;
                return this.ready();

              case 10:
                if (!_classPrivateFieldGet(this, _index)[row._id]) {
                  _context17.next = 12;
                  break;
                }

                throw new Error('Row must not already exist');

              case 12:
                return _context17.abrupt("return", this[_append](row));

              case 13:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17, this);
      }));

      function append(_x16) {
        return _append2.apply(this, arguments);
      }

      return append;
    }()
  }, {
    key: "get",
    value: function () {
      var _get2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee18(rowNumberOrId) {
        var _ref10, block, offset;

        return regeneratorRuntime.wrap(function _callee18$(_context18) {
          while (1) {
            switch (_context18.prev = _context18.next) {
              case 0:
                if (!(rowNumberOrId === undefined)) {
                  _context18.next = 2;
                  break;
                }

                throw new Error('Invalid row');

              case 2:
                if (!(0, _isString.default)(rowNumberOrId)) {
                  _context18.next = 8;
                  break;
                }

                _context18.next = 5;
                return this.ready();

              case 5:
                return _context18.abrupt("return", this.get(_classPrivateFieldGet(this, _index)[rowNumberOrId]));

              case 8:
                _context18.next = 10;
                return this.getBlockForRow(rowNumberOrId);

              case 10:
                _ref10 = _context18.sent;
                block = _ref10.block;
                offset = _ref10.offset;
                return _context18.abrupt("return", block.rows[offset]);

              case 14:
              case "end":
                return _context18.stop();
            }
          }
        }, _callee18, this);
      }));

      function get(_x17) {
        return _get2.apply(this, arguments);
      }

      return get;
    }()
  }, {
    key: "getRowNumber",
    value: function getRowNumber(id) {
      return _classPrivateFieldGet(this, _index)[id];
    }
  }, {
    key: "set",
    value: function () {
      var _set = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee19(rows) {
        return regeneratorRuntime.wrap(function _callee19$(_context19) {
          while (1) {
            switch (_context19.prev = _context19.next) {
              case 0:
                _context19.next = 2;
                return this.clear();

              case 2:
                return _context19.abrupt("return", this[_append](rows));

              case 3:
              case "end":
                return _context19.stop();
            }
          }
        }, _callee19, this);
      }));

      function set(_x18) {
        return _set.apply(this, arguments);
      }

      return set;
    }()
  }, {
    key: "configure",
    value: function () {
      var _configure = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee21() {
        var _this7 = this;

        var options,
            _args21 = arguments;
        return regeneratorRuntime.wrap(function _callee21$(_context21) {
          while (1) {
            switch (_context21.prev = _context21.next) {
              case 0:
                options = _args21.length > 0 && _args21[0] !== undefined ? _args21[0] : {};
                _context21.next = 3;
                return this.clear();

              case 3:
                options.rowCount = options.rowCount || (options.rows || []).length;
                this.settings = Object.assign(this.settings, {
                  block_size: options.blockSize || 256,
                  rowCount: options.rowCount || 0
                });

                if (!options.rows) {
                  _context21.next = 11;
                  break;
                }

                if (!(options.rows.length !== options.rowCount && options.rows.length < _classPrivateFieldGet(this, _block_size))) {
                  _context21.next = 8;
                  break;
                }

                throw new Error('Invalid number of rows, must be all rows or more than a block');

              case 8:
                options.rows.length = Math.max(options.rowCount, Math.floor(options.rows.length / _classPrivateFieldGet(this, _block_size)) * _classPrivateFieldGet(this, _block_size));
                _context21.next = 11;
                return Promise.all(options.rows.map(
                /*#__PURE__*/
                function () {
                  var _ref11 = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee20(row, index) {
                    return regeneratorRuntime.wrap(function _callee20$(_context20) {
                      while (1) {
                        switch (_context20.prev = _context20.next) {
                          case 0:
                            return _context20.abrupt("return", _this7[setRow](index, row, true));

                          case 1:
                          case "end":
                            return _context20.stop();
                        }
                      }
                    }, _callee20, this);
                  }));

                  return function (_x19, _x20) {
                    return _ref11.apply(this, arguments);
                  };
                }()));

              case 11:
                this[refreshed]();

              case 12:
              case "end":
                return _context21.stop();
            }
          }
        }, _callee21, this);
      }));

      function configure() {
        return _configure.apply(this, arguments);
      }

      return configure;
    }()
  }, {
    key: "find",
    value: function () {
      var _find = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee22(query, limit) {
        var start,
            includeDeleted,
            qry,
            rows,
            _this$settings2,
            rowCount,
            block_size,
            remaining,
            row,
            _ref12,
            block,
            cursor,
            _args22 = arguments;

        return regeneratorRuntime.wrap(function _callee22$(_context22) {
          while (1) {
            switch (_context22.prev = _context22.next) {
              case 0:
                start = _args22.length > 2 && _args22[2] !== undefined ? _args22[2] : 0;
                includeDeleted = _args22.length > 3 && _args22[3] !== undefined ? _args22[3] : false;
                qry = new _mingo.default.Query(query);
                rows = [];
                _this$settings2 = this.settings, rowCount = _this$settings2.rowCount, block_size = _this$settings2.block_size;
                limit = Math.min(limit || rowCount, rowCount);
                remaining = limit;
                row = start;

              case 8:
                if (!(row < limit)) {
                  _context22.next = 18;
                  break;
                }

                _context22.next = 11;
                return this.getBlockForRow(row);

              case 11:
                _ref12 = _context22.sent;
                block = _ref12.block;
                cursor = qry.find(block.rows.slice(0, remaining).filter(function (item) {
                  return !item._deleted || includeDeleted;
                }));
                rows.push(cursor.all());

              case 15:
                row += block_size, remaining -= block_size;
                _context22.next = 8;
                break;

              case 18:
                return _context22.abrupt("return", (0, _flatten.default)(rows));

              case 19:
              case "end":
                return _context22.stop();
            }
          }
        }, _callee22, this);
      }));

      function find(_x21, _x22) {
        return _find.apply(this, arguments);
      }

      return find;
    }()
  }, {
    key: "stream",
    value: function stream(query) {
      var _this8 = this;

      var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var includeDeleted = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var row = start;
      var qry = new _mingo.default.Query(query);
      var settings = this.settings;
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee23() {
          var _item2;

          return regeneratorRuntime.wrap(function _callee23$(_context23) {
            while (1) {
              switch (_context23.prev = _context23.next) {
                case 0:
                  _context23.prev = 0;

                case 1:
                  _context23.next = 3;
                  return _this8.get(row++);

                case 3:
                  _item2 = _context23.sent;
                  if (_item2 && _item2._deleted && !includeDeleted) _item2 = null;
                  if (_item2 && !qry.test(_item2)) _item2 = null;

                  if (!_item2) {
                    _context23.next = 8;
                    break;
                  }

                  return _context23.abrupt("return", _item2);

                case 8:
                  if (!_item2 && row < settings.rowCount) {
                    _context23.next = 1;
                    break;
                  }

                case 9:
                  return _context23.abrupt("return", null);

                case 10:
                  _context23.prev = 10;
                  _context23.next = 13;
                  return _this8.cleanBlocks(_this8.settings.holdBlocks || 2, 10);

                case 13:
                  return _context23.finish(10);

                case 14:
                case "end":
                  return _context23.stop();
              }
            }
          }, _callee23, this, [[0,, 10, 14]]);
        }))
      );
    }
  }, {
    key: "compact",
    value: function () {
      var _compact = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee24() {
        return regeneratorRuntime.wrap(function _callee24$(_context24) {
          while (1) {
            switch (_context24.prev = _context24.next) {
              case 0:
                _context24.next = 2;
                return this.writeBlocks();

              case 2:
                _context24.next = 4;
                return this.cleanBlocks(0, -10);

              case 4:
              case "end":
                return _context24.stop();
            }
          }
        }, _callee24, this);
      }));

      function compact() {
        return _compact.apply(this, arguments);
      }

      return compact;
    }()
  }, {
    key: "all",
    value: function () {
      var _all = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee25(limit) {
        var start,
            includeDeleted,
            rows,
            _this$settings3,
            rowCount,
            block_size,
            remaining,
            row,
            _ref14,
            block,
            _args25 = arguments;

        return regeneratorRuntime.wrap(function _callee25$(_context25) {
          while (1) {
            switch (_context25.prev = _context25.next) {
              case 0:
                start = _args25.length > 1 && _args25[1] !== undefined ? _args25[1] : 0;
                includeDeleted = _args25.length > 2 && _args25[2] !== undefined ? _args25[2] : false;
                rows = [];
                _context25.next = 5;
                return this.ready();

              case 5:
                _this$settings3 = this.settings, rowCount = _this$settings3.rowCount, block_size = _this$settings3.block_size;
                limit = Math.min(limit || rowCount, rowCount);
                remaining = limit;
                row = start;

              case 9:
                if (!(row < limit)) {
                  _context25.next = 18;
                  break;
                }

                _context25.next = 12;
                return this.getBlockForRow(row);

              case 12:
                _ref14 = _context25.sent;
                block = _ref14.block;
                rows.push(block.rows.slice(0, remaining).filter(function (item) {
                  return !item._deleted || includeDeleted;
                }));

              case 15:
                row += block_size, remaining -= block_size;
                _context25.next = 9;
                break;

              case 18:
                return _context25.abrupt("return", (0, _flatten.default)(rows));

              case 19:
              case "end":
                return _context25.stop();
            }
          }
        }, _callee25, this);
      }));

      function all(_x23) {
        return _all.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "rowCount",
    get: function get() {
      return this.settings.rowCount;
    }
  }, {
    key: "settings",
    get: function get() {
      return _classPrivateFieldSet(this, _settings, _classPrivateFieldGet(this, _settings) || this.adaptor.getSettings(this.name).then(function (result) {
        return result || {
          block_size: 0,
          rowCount: 0
        };
      }));
    },
    set: function set(value) {
      _classPrivateFieldSet(this, _settings, value);

      return this.adaptor.setSettings(name, value);
    }
  }]);

  return DataSource;
}(_baseDataSource.default);

exports.DataSource = DataSource;

var _block_size = new WeakMap();

var _settings = new WeakMap();

var _blocks = new WeakMap();

var _index = new WeakMap();

var _timer = new WeakMap();

var _default = DataSource;
exports.default = _default;