"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.BaseDataSource = exports.dataSources = exports.ready = void 0;

var _eventemitter = require("eventemitter2");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var _ready = Symbol('ready');

exports.ready = _ready;
var dataSources = {};
exports.dataSources = dataSources;

var BaseDataSource =
/*#__PURE__*/
function (_EventEmitter) {
  _inherits(BaseDataSource, _EventEmitter);

  _createClass(BaseDataSource, null, [{
    key: "get",
    value: function get(name) {
      var result = dataSources[name] || null;
      if (!result) throw new Error('Nothing to create');
      return result;
    }
  }, {
    key: "byName",
    get: function get() {
      return dataSources;
    }
  }]);

  function BaseDataSource(name) {
    var _this;

    _classCallCheck(this, BaseDataSource);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BaseDataSource).call(this, {
      maxListeners: 100,
      wildcard: true
    }));

    if (dataSources[name]) {
      console.log(Object.keys(dataSources));
      throw new Error("A data source with that name already exists");
    }

    dataSources[name] = _assertThisInitialized(_assertThisInitialized(_this));
    _this.name = name;
    dataSources[name] = _assertThisInitialized(_assertThisInitialized(_this));
    _this[_ready] = Promise.resolve(true);
    return _this;
  }

  _createClass(BaseDataSource, [{
    key: "ready",
    value: function () {
      var _ready2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this[_ready];

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function ready() {
        return _ready2.apply(this, arguments);
      }

      return ready;
    }()
  }, {
    key: "getSources",
    value: function getSources(list, type) {
      if (type === this.name) list.push(this);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      delete dataSources[this.name];
    }
  }, {
    key: "get",
    value: function () {
      var _get = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(rowNumberOrId) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(rowNumberOrId === undefined)) {
                  _context2.next = 2;
                  break;
                }

                throw new Error('Invalid row');

              case 2:
                throw new Error('Not found');

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function get(_x) {
        return _get.apply(this, arguments);
      }

      return get;
    }()
  }, {
    key: "find",
    value: function () {
      var _find = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(query, limit, start) {
        var includeDeleted,
            _args3 = arguments;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                includeDeleted = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : false;
                return _context3.abrupt("return", []);

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function find(_x2, _x3, _x4) {
        return _find.apply(this, arguments);
      }

      return find;
    }()
  }, {
    key: "stream",
    value: function stream(query) {
      var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var includeDeleted = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      //Return an async function that will return a record or null if no more match
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee4() {
          return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                case "end":
                  return _context4.stop();
              }
            }
          }, _callee4, this);
        }))
      );
    }
  }, {
    key: "all",
    value: function () {
      var _all = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(limit, start) {
        var includeDeleted,
            _args5 = arguments;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                includeDeleted = _args5.length > 2 && _args5[2] !== undefined ? _args5[2] : false;
                return _context5.abrupt("return", []);

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function all(_x5, _x6) {
        return _all.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "rowCount",
    get: function get() {
      return this.settings.rowCount;
    }
  }]);

  return BaseDataSource;
}(_eventemitter.EventEmitter2);

exports.BaseDataSource = BaseDataSource;
var _default = BaseDataSource;
exports.default = _default;