"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.adaptor = void 0;

var _localforage = _interopRequireDefault(require("localforage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var adaptor = {
  getBlock: function () {
    var _getBlock = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(name, block) {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _localforage.default.getItem("block-".concat(name, "-").concat(block));

            case 2:
              return _context.abrupt("return", _context.sent);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function getBlock(_x, _x2) {
      return _getBlock.apply(this, arguments);
    }

    return getBlock;
  }(),
  setBlock: function () {
    var _setBlock = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(name, block, data) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _localforage.default.setItem("block-".concat(name, "-").concat(block), data);

            case 2:
              return _context2.abrupt("return", _context2.sent);

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function setBlock(_x3, _x4, _x5) {
      return _setBlock.apply(this, arguments);
    }

    return setBlock;
  }(),
  removeBlock: function () {
    var _removeBlock = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(name, block) {
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return _localforage.default.removeItem("block-".concat(name, "-").concat(block));

            case 2:
              return _context3.abrupt("return", _context3.sent);

            case 3:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function removeBlock(_x6, _x7) {
      return _removeBlock.apply(this, arguments);
    }

    return removeBlock;
  }(),
  getSettings: function () {
    var _getSettings = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4(name) {
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return _localforage.default.getItem("settings-".concat(name));

            case 2:
              return _context4.abrupt("return", _context4.sent);

            case 3:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function getSettings(_x8) {
      return _getSettings.apply(this, arguments);
    }

    return getSettings;
  }(),
  setSettings: function () {
    var _setSettings = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5(name, value) {
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _localforage.default.setItem("settings-".concat(name), value);

            case 2:
              return _context5.abrupt("return", _context5.sent);

            case 3:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, this);
    }));

    function setSettings(_x9, _x10) {
      return _setSettings.apply(this, arguments);
    }

    return setSettings;
  }(),
  getIndex: function () {
    var _getIndex = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee6(name) {
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.next = 2;
              return _localforage.default.getItem("index-".concat(name));

            case 2:
              return _context6.abrupt("return", _context6.sent);

            case 3:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, this);
    }));

    function getIndex(_x11) {
      return _getIndex.apply(this, arguments);
    }

    return getIndex;
  }(),
  setIndex: function () {
    var _setIndex = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee7(name, value) {
      return regeneratorRuntime.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              _context7.next = 2;
              return _localforage.default.setItem("index-".concat(name), value);

            case 2:
              return _context7.abrupt("return", _context7.sent);

            case 3:
            case "end":
              return _context7.stop();
          }
        }
      }, _callee7, this);
    }));

    function setIndex(_x12, _x13) {
      return _setIndex.apply(this, arguments);
    }

    return setIndex;
  }()
};
exports.adaptor = adaptor;
var _default = adaptor;
exports.default = _default;