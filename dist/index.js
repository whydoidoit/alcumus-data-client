"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "DataSource", {
  enumerable: true,
  get: function get() {
    return _datasource.default;
  }
});
Object.defineProperty(exports, "RemoteDataSource", {
  enumerable: true,
  get: function get() {
    return _remoteDatasource.default;
  }
});
Object.defineProperty(exports, "enableRemoteMutations", {
  enumerable: true,
  get: function get() {
    return _remoteMutations.enable;
  }
});
Object.defineProperty(exports, "disableRemoteMutations", {
  enumerable: true,
  get: function get() {
    return _remoteMutations.disable;
  }
});
Object.defineProperty(exports, "flushRemoteMutations", {
  enumerable: true,
  get: function get() {
    return _remoteMutations.flush;
  }
});
Object.defineProperty(exports, "track", {
  enumerable: true,
  get: function get() {
    return _datasourceMutations.track;
  }
});
Object.defineProperty(exports, "remoteDataUrl", {
  enumerable: true,
  get: function get() {
    return _urls.url;
  }
});
Object.defineProperty(exports, "setRemoteDataUrl", {
  enumerable: true,
  get: function get() {
    return _urls.setRemoteDataUrl;
  }
});
exports.default = void 0;

require("regenerator-runtime/runtime");

var _datasource = _interopRequireDefault(require("./datasource"));

var _remoteDatasource = _interopRequireDefault(require("./remote-datasource"));

var _remoteMutations = require("./remote-mutations");

var _datasourceMutations = require("./datasource-mutations");

var _urls = require("./urls");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _remoteDatasource.default;
exports.default = _default;