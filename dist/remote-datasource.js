"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setFetchImplementation = setFetchImplementation;
exports.setEventSourceImplementation = setEventSourceImplementation;
exports.default = exports.RemoteDataSource = void 0;

var _datasource = require("./datasource");

var _baseDataSource = require("./base-data-source");

var _alcumusAppDatasourceMutations = require("alcumus-app-datasource-mutations");

var _querystring = require("querystring");

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _mingo = _interopRequireDefault(require("mingo"));

var _eventemitter = require("eventemitter2");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _classPrivateFieldGet(receiver, privateMap) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return privateMap.get(receiver).value; }

function _classPrivateFieldSet(receiver, privateMap, value) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to set private field on non-instance"); } var descriptor = privateMap.get(receiver); if (!descriptor.writable) { throw new TypeError("attempted to set read only private field"); } descriptor.value = value; return value; }

var fetchApi = window.fetch;
var EventSourceApi = window.EventSource;

function setFetchImplementation(fetchImplementation) {
  fetchApi = fetchImplementation;
}

function setEventSourceImplementation(EventSourceImplementation) {
  EventSourceApi = EventSourceImplementation;
}

var remoteEvents = new _eventemitter.EventEmitter2({
  wildcard: true,
  maxListeners: 0
}); //Wire the remote mutate event into the datasource-mutations mutated event

remoteEvents.on('mutate', function (mutation) {
  _alcumusAppDatasourceMutations.events.emit('mutated', {
    mutation: mutation
  });
});
var databaseTrackers = {};

function trackDatabase(url, database) {
  var lastId = localStorage.getItem("event-id-".concat(database));
  var eventSource = new EventSourceApi("".concat(url, "/updates/").concat(database).concat(lastId ? '?lastEventId=' + lastId : ''));
  eventSource.addEventListener('message', function (message) {
    if (message.lastEventId) {
      localStorage.setItem("event-id-".concat(database), message.lastEventId);
    }

    var packet = JSON.parse(message.data);
    remoteEvents.emit(packet.event, packet.message);
  });
}

var RemoteDataSource =
/*#__PURE__*/
function (_DataSource) {
  _inherits(RemoteDataSource, _DataSource);

  _createClass(RemoteDataSource, null, [{
    key: "events",
    value: function events() {
      return remoteEvents;
    }
  }, {
    key: "get",
    value: function get(name, url, source, options) {
      return _baseDataSource.dataSources[name] || new RemoteDataSource(name, url, source, options);
    }
  }]);

  function RemoteDataSource(name, _url2, _source2) {
    var _this;

    var _ref = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
        _ref$refresh = _ref.refresh,
        refresh = _ref$refresh === void 0 ? false : _ref$refresh,
        _ref$cacheAll = _ref.cacheAll,
        _cacheAll2 = _ref$cacheAll === void 0 ? true : _ref$cacheAll,
        _query2 = _ref.query,
        props = _objectWithoutProperties(_ref, ["refresh", "cacheAll", "query"]);

    _classCallCheck(this, RemoteDataSource);

    if (!name) throw new Error('Must have a name');
    if (!_url2) throw new Error('You must supply a url');
    if (!_source2 || _source2.split('/').length !== 2) throw new Error('Source must be <database>/<table>');
    _this = _possibleConstructorReturn(this, _getPrototypeOf(RemoteDataSource).call(this, name, props));

    _resolve.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _reject.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _promise.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _url.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _source.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: null
    });

    _cacheAll.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: false
    });

    _query.set(_assertThisInitialized(_assertThisInitialized(_this)), {
      writable: true,
      value: undefined
    });

    var _source2$split = _source2.split('/'),
        _source2$split2 = _slicedToArray(_source2$split, 1),
        database = _source2$split2[0];

    databaseTrackers[database] = databaseTrackers[database] || trackDatabase(_url2, database);
    if ((0, _isObject.default)(_query2)) _query2 = JSON.stringify(_query2);

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _promise, new Promise(function (resolve, reject) {
      _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _resolve, resolve);

      _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _reject, reject);
    }));

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _url, _url2);

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _source, _source2);

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _cacheAll, _cacheAll2);

    _classPrivateFieldSet(_assertThisInitialized(_assertThisInitialized(_this)), _query, _query2);

    var configure =
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var settings;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this[_baseDataSource.ready];

              case 2:
                settings = _this.settings;
                settings.url = _url2;
                settings.source = _source2;
                settings.refresh = refresh;
                settings.cacheAll = _cacheAll2;
                settings.query = _query2;
                _this.settings = settings;

                _this.on('retrieve', _this.retrieveBlock = _this.retrieveBlock.bind(_assertThisInitialized(_assertThisInitialized(_this))));

                remoteEvents.on('refresh-data', _this.refreshData = _this.refreshData.bind(_assertThisInitialized(_assertThisInitialized(_this))));
                remoteEvents.on('remove', function (id) {
                  this.eraseRow(id).catch(console.error);
                });

                if (!(!settings.retrieved || refresh)) {
                  _context.next = 17;
                  break;
                }

                _context.next = 15;
                return _this.refresh();

              case 15:
                _context.next = 18;
                break;

              case 17:
                _classPrivateFieldGet(_assertThisInitialized(_assertThisInitialized(_this)), _resolve).call(_assertThisInitialized(_assertThisInitialized(_this)));

              case 18:
                return _context.abrupt("return", "Configured ".concat(_source2, "@").concat(_url2));

              case 19:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function configure() {
        return _ref2.apply(this, arguments);
      };
    }();

    configure().then(function () {}, console.error);
    return _this;
  }

  _createClass(RemoteDataSource, [{
    key: "refreshData",
    value: function refreshData(message) {
      try {
        if (message === 'all' || message === _classPrivateFieldGet(this, _source) || message === this.name) {
          return this.refresh();
        }
      } catch (e) {}
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var withoutClearing = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.off('retrieve', this.retrieveBlock);
      remoteEvents.off('refresh-data', this.refreshData);
      return _get(_getPrototypeOf(RemoteDataSource.prototype), "destroy", this).call(this, withoutClearing);
    }
  }, {
    key: "get",
    value: function () {
      var _get2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(rowOrId) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.initialized;

              case 2:
                _context2.next = 4;
                return _get(_getPrototypeOf(RemoteDataSource.prototype), "get", this).call(this, rowOrId);

              case 4:
                return _context2.abrupt("return", _context2.sent);

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function get(_x) {
        return _get2.apply(this, arguments);
      }

      return get;
    }()
  }, {
    key: "ready",
    value: function () {
      var _ready2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this[_baseDataSource.ready];

              case 2:
                _context3.next = 4;
                return this.initialized;

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function ready() {
        return _ready2.apply(this, arguments);
      }

      return ready;
    }()
  }, {
    key: "shouldContain",
    value: function shouldContain(type, mutation) {
      var canContain = _get(_getPrototypeOf(RemoteDataSource.prototype), "shouldContain", this).call(this, type) || type === _classPrivateFieldGet(this, _source);

      if (canContain && mutation.type === 'create') {
        return !_classPrivateFieldGet(this, _query) || (this._query = this._query || new _mingo.default.Query(JSON.parse(_classPrivateFieldGet(this, _query)))).test(mutation.state);
      } else {
        return false;
      }
    }
  }, {
    key: "getSources",
    value: function getSources(list, type) {
      _get(_getPrototypeOf(RemoteDataSource.prototype), "getSources", this).call(this, list, type);

      if (type === _classPrivateFieldGet(this, _source)) list.push(this);
    }
  }, {
    key: "retrieveBlock",
    value: function () {
      var _retrieveBlock = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(_ref3) {
        var startRow, blockSize, rows, settings, query, response, result;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                startRow = _ref3.startRow, blockSize = _ref3.blockSize, rows = _ref3.rows;
                settings = this.settings;
                query = Object.assign({}, {
                  skip: startRow,
                  take: blockSize
                });
                if (settings.query) query.query = settings.query;
                _context4.next = 6;
                return fetchApi("".concat(_classPrivateFieldGet(this, _url), "/data/").concat(settings.source, "?").concat((0, _querystring.stringify)(query)), {
                  credentials: 'include',
                  cache: 'no-cache'
                });

              case 6:
                response = _context4.sent;

                if (!response.ok) {
                  _context4.next = 14;
                  break;
                }

                _context4.next = 10;
                return response.json();

              case 10:
                result = _context4.sent;
                Array.prototype.push.apply(rows, result.rows);
                _context4.next = 15;
                break;

              case 14:
                throw new Error('Unable to retrieve block');

              case 15:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function retrieveBlock(_x2) {
        return _retrieveBlock.apply(this, arguments);
      }

      return retrieveBlock;
    }()
  }, {
    key: "getMissingRecord",
    value: function () {
      var _getMissingRecord = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(id) {
        var response, records;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return fetchApi("".concat(_classPrivateFieldGet(this, _url), "/data/").concat(this.settings.source, "?").concat((0, _querystring.stringify)({
                  query: {
                    _id: id
                  },
                  take: 1
                })), {
                  credentials: 'include',
                  cache: 'no-cache'
                });

              case 3:
                response = _context5.sent;

                if (!(response && response.ok)) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return response.json();

              case 7:
                records = _context5.sent;
                return _context5.abrupt("return", records[0]);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                console.error(_context5.t0);
                return _context5.abrupt("return", null);

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 12]]);
      }));

      function getMissingRecord(_x3) {
        return _getMissingRecord.apply(this, arguments);
      }

      return getMissingRecord;
    }()
  }, {
    key: "refresh",
    value: function () {
      var _refresh = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6() {
        var settings, numberOfRows, query, response;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this[_baseDataSource.ready];

              case 2:
                settings = this.settings;

                if (!(!settings.source && !_classPrivateFieldGet(this, _source))) {
                  _context6.next = 7;
                  break;
                }

                console.warn('Source is empty');

                _classPrivateFieldGet(this, _resolve).call(this);

                return _context6.abrupt("return");

              case 7:
                numberOfRows = settings.block_size * settings.holdBlocks;
                query = Object.assign({});
                if (settings.query) query.query = settings.query;
                if (!settings.cacheAll) query.take = numberOfRows;
                _context6.next = 13;
                return fetchApi("".concat(_classPrivateFieldGet(this, _url), "/data/").concat(settings.source, "?").concat((0, _querystring.stringify)(query)), {
                  credentials: 'include',
                  cache: 'no-cache'
                });

              case 13:
                response = _context6.sent;

                if (!(response && response.ok)) {
                  _context6.next = 27;
                  break;
                }

                _context6.t0 = this;
                _context6.next = 18;
                return response.json();

              case 18:
                _context6.t1 = _context6.sent;
                _context6.next = 21;
                return _context6.t0.configure.call(_context6.t0, _context6.t1);

              case 21:
                settings = this.settings;
                settings.retrieved = true;
                this.settings = settings;

                _classPrivateFieldGet(this, _resolve).call(this);

                _context6.next = 30;
                break;

              case 27:
                console.log('Failed', response);

                _classPrivateFieldGet(this, _reject).call(this, "Failed to retrieve ".concat(this.name, " from ").concat(_classPrivateFieldGet(this, _url), " (").concat(_classPrivateFieldGet(this, _source), ")"));

                throw new Error("Failed to retrieve ".concat(this.name, " from ").concat(_classPrivateFieldGet(this, _url), " (").concat(_classPrivateFieldGet(this, _source), ")"));

              case 30:
                this.emit('refreshed');
                this.emit('updated');

              case 32:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function refresh() {
        return _refresh.apply(this, arguments);
      }

      return refresh;
    }()
  }, {
    key: "initialized",
    get: function get() {
      return _classPrivateFieldGet(this, _promise);
    }
  }]);

  return RemoteDataSource;
}(_datasource.DataSource);

exports.RemoteDataSource = RemoteDataSource;

var _resolve = new WeakMap();

var _reject = new WeakMap();

var _promise = new WeakMap();

var _url = new WeakMap();

var _source = new WeakMap();

var _cacheAll = new WeakMap();

var _query = new WeakMap();

var _default = RemoteDataSource;
exports.default = _default;