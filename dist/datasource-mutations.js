"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.track = track;

var _alcumusAppDatasourceMutations = require("alcumus-app-datasource-mutations");

var _uniq = _interopRequireDefault(require("lodash/uniq"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

_alcumusAppDatasourceMutations.events.on("mutated",
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(_ref) {
    var mutation, copies, _mutation$_id$split, _mutation$_id$split2, type, list;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            mutation = _ref.mutation;
            copies = [];
            _mutation$_id$split = mutation._id.split(':'), _mutation$_id$split2 = _slicedToArray(_mutation$_id$split, 2), type = _mutation$_id$split2[1];
            list = [];
            _context.next = 6;
            return _alcumusAppDatasourceMutations.events.emitAsync("data-source.".concat(type), list, type);

          case 6:
            _context.next = 8;
            return _alcumusAppDatasourceMutations.events.emitAsync("post.data-source.".concat(type), list, type);

          case 8:
            _context.next = 10;
            return _alcumusAppDatasourceMutations.events.emitAsync("get.".concat(mutation._id), copies, mutation);

          case 10:
            (0, _uniq.default)(copies).forEach(function (copy) {
              try {
                (0, _alcumusAppDatasourceMutations.applyMutation)(copy, mutation, false);
              } catch (e) {
                console.error(e);
              }
            });

            _alcumusAppDatasourceMutations.events.emit("updated.".concat(mutation._id));

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref2.apply(this, arguments);
  };
}());

function track(item, callback) {
  var fn = function fn(items) {
    items.push(item);
  };

  _alcumusAppDatasourceMutations.events.on("get.".concat(item._id), fn);

  callback && _alcumusAppDatasourceMutations.events.on("updated.".concat(item._id), callback);
  return function () {
    _alcumusAppDatasourceMutations.events.removeListener("get.".concat(item._id), fn);

    callback && _alcumusAppDatasourceMutations.events.removeListener("updated.".concat(item._id), callback);
  };
}