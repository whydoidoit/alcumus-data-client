"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.boot = boot;
exports.enable = enable;
exports.disable = disable;
exports.flush = flush;

var _alcumusAppDatasourceMutations = require("alcumus-app-datasource-mutations");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _urls = require("./urls");

var _localforage = _interopRequireDefault(require("localforage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var url = _urls.url;
var delay = 1;
window.addEventListener("online", processMessages);
window.addEventListener("activate", processMessages);

function processMessages() {
  delay = 1;
  if (!(0, _isEmpty.default)(_alcumusAppDatasourceMutations.mutations)) process();
}

function boot() {
  return _boot.apply(this, arguments);
}

function _boot() {
  _boot = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3() {
    var mutations;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _localforage.default.getItem("_stored_mutations");

          case 2:
            mutations = _context3.sent;

            if (!(!mutations || (0, _isEmpty.default)(mutations))) {
              _context3.next = 5;
              break;
            }

            return _context3.abrupt("return", "NO STORED MUTATIONS");

          case 5:
            Object.entries(mutations).forEach(function (_ref4) {
              var _ref5 = _slicedToArray(_ref4, 2),
                  key = _ref5[0],
                  mutation = _ref5[1];

              _alcumusAppDatasourceMutations.mutations[key] = [].concat(_toConsumableArray(mutation), _toConsumableArray(_alcumusAppDatasourceMutations.mutations[key] || []));
            });
            process();
            return _context3.abrupt("return", "QUEUED STORED MUTATIONS");

          case 8:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));
  return _boot.apply(this, arguments);
}

var storing = null;
var processOffline = (0, _debounce.default)(
/*#__PURE__*/
function () {
  var _processOffline = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(_ref) {
    var mutations;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            mutations = _ref.mutations;

            if (!(navigator.onLine === false)) {
              _context.next = 5;
              break;
            }

            _context.next = 4;
            return Promise.resolve(storing);

          case 4:
            storing = _localforage.default.setItem("_stored_mutations", mutations);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  function processOffline(_x) {
    return _processOffline.apply(this, arguments);
  }

  return processOffline;
}(), 150);
var process = (0, _debounce.default)(
/*#__PURE__*/
function () {
  var _process = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    var mutations;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            mutations = _alcumusAppDatasourceMutations.mutations;
            _context2.prev = 1;
            _context2.next = 4;
            return _localforage.default.setItem("_stored_mutations", mutations);

          case 4:
            if (!navigator.onLine) {
              _context2.next = 12;
              break;
            }

            (0, _alcumusAppDatasourceMutations.clear)();

            if ((0, _isEmpty.default)(mutations)) {
              _context2.next = 9;
              break;
            }

            _context2.next = 9;
            return fetch("".concat(url, "/mutate"), {
              method: "POST",
              mode: "cors",
              cache: "no-cache",
              credentials: "include",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(mutations)
            });

          case 9:
            delay = 1;
            _context2.next = 12;
            return _localforage.default.removeItem("_stored_mutations");

          case 12:
            _context2.next = 19;
            break;

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](1);
            console.error(_context2.t0.stack); //If we have an error we need to recombine

            Object.entries(mutations).forEach(function (_ref2) {
              var _ref3 = _slicedToArray(_ref2, 2),
                  key = _ref3[0],
                  mutation = _ref3[1];

              _alcumusAppDatasourceMutations.mutations[key] = [].concat(_toConsumableArray(mutation), _toConsumableArray(_alcumusAppDatasourceMutations.mutations[key] || []));
            });
            setTimeout(process, (delay = Math.min(30, delay + 1)) * 1000);

          case 19:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this, [[1, 14]]);
  }));

  function process() {
    return _process.apply(this, arguments);
  }

  return process;
}(), 750, {
  maxWait: 15000
});
var enabled = false;

function enable() {
  var useUrl = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _urls.url;
  url = useUrl;
  if (enabled) return;
  enabled = true;

  _alcumusAppDatasourceMutations.events.on("mutated", process);

  _alcumusAppDatasourceMutations.events.on("mutated", processOffline);
}

function disable() {
  if (!enabled) return;
  enabled = false;

  _alcumusAppDatasourceMutations.events.removeListener("mutated", process);

  _alcumusAppDatasourceMutations.events.removeListener("mutated", processOffline);

  module.exports.flush();
}

function flush() {
  process.flush();
}

boot().then(console.log);