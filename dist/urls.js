"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setRemoteDataUrl = setRemoteDataUrl;
exports.url = void 0;
var url = window ? window.url : global ? global.url : "http://localhost:3000";
exports.url = url;

function setRemoteDataUrl(newUrl) {
  exports.url = url = newUrl;
}